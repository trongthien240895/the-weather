//
//  NibLoadView.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//


import UIKit

public protocol NibLoadable: Any {
    func nibName() -> String
    func bundle() -> Bundle
    func loadFromNib()
}

public extension NibLoadable {
    func nibName() -> String {
        return String(describing: type(of: self))
    }
    
    func bundle() -> Bundle {
        return Bundle(for: type(of: self as AnyObject))
    }
    
    func loadFromNib() {
        let nib = UINib(nibName: nibName(), bundle: bundle())
        nib.instantiate(withOwner: self, options: nil)
    }
}

public protocol NibLoadableViewType: NibLoadable {
    var viewContent: UIView! { get }
    func loadContentViewFromNib()
}

public extension NibLoadableViewType where Self: UIView {
    func loadContentViewFromNib() {
        loadFromNib()
        
        viewContent.backgroundColor = UIColor.clear
        addSubview(viewContent)
        viewContent.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                           options: [],
                                           metrics: nil,
                                           views: ["view": viewContent])
        )
        addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                           options: [],
                                           metrics: nil,
                                           views: ["view": viewContent])
        )
    }
}
