//
//  Extention.swift
//  the-weather
//
//  Created by Mac Pro on 7/13/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import Foundation

enum DateFormat: String {
    
    case dayOfWeek = "EEEE"
    case dateType1 = "dd-MM-yyyy"
    case dateType2 = "HH:mm"
    
}

extension Date {
    
    func dayOfWeek(_ dateType: DateFormat) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateType.rawValue
        return dateFormatter.string(from: self).capitalized
    }
    
}
