//
//  LocationServices.swift
//  the-weather
//
//  Created by Mac Pro on 6/23/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import Foundation
import MapKit

typealias JSONDictionary = [String:Any]

class LocationServices: NSObject {
    
    static let shared = LocationServices()
    private var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    let authStatus = CLLocationManager.authorizationStatus()
    let inUse = CLAuthorizationStatus.authorizedWhenInUse
    let always = CLAuthorizationStatus.authorizedAlways
    
    private override init() {
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.requestAlwaysAuthorization()
            locationManager?.startUpdatingLocation()
        }
        currentLocation = locationManager?.location
    }
    
    func getAdress(completion: @escaping (_ address: JSONDictionary?, _ error: Error?) -> ()) {
        guard let currentLocation = currentLocation else {
            return
        }
        CLGeocoder().reverseGeocodeLocation(currentLocation) { placemarks, error in
            guard let placemarks = placemarks,
                let address = placemarks[0].addressDictionary as? JSONDictionary,
                error == nil else {
                return
            }
            completion(address, nil)
        }
    }

}
