//
//  ViewController.swift
//  the-weather
//
//  Created by Mac Pro on 6/23/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit
import SwiftSky
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var iconForecast: UIImageView!
    var locationService: LocationServices?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationService = LocationServices.shared
        locationService?.getAdress { address, error in
            if let a = address, let city = a["City"] as? String {
                print(city)
//                print(self.locationService?.currentLocation?.coordinate.latitude)
            }
        }
        
        SwiftSky.units.temperature = .celsius

        SwiftSky.get([.current, .minutes, .hours, .days, .alerts],
                     at: Location(latitude: 1.1234, longitude: 1.234)
        ) { result in
            switch result {
            case .success(let forecast):
                print(forecast)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    

    
}
