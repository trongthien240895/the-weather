//
//  AppDelegate.swift
//  the-weather
//
//  Created by Mac Pro on 6/23/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit
import SwiftSky

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        SwiftSky.secret = "a643fed8cf0518d2d8211fb5f0298dff"
        LocationServices.shared
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
     
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       
    }

    func applicationWillTerminate(_ application: UIApplication) {
    
    }

}
