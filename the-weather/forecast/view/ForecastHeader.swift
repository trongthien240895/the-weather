//
//  ForecastHeader.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit

class ForecastHeader: UIView, NibLoadableViewType {

    @IBOutlet var viewContent: UIView!
    @IBOutlet weak var forecastImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        loadContentViewFromNib()
    }

}
