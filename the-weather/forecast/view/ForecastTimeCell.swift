//
//  ForecastTimeCell.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit

class ForecastTimeCell: UIView, NibLoadableViewType {
    
    @IBOutlet var viewContent: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var forecastImage: UIImageView!
    @IBOutlet weak var tempatureLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        loadContentViewFromNib()
    }
    
    func initCell(_ time: String, icon: String, tempature: Double) {
        timeLabel.text = time
        forecastImage.image = UIImage(named: icon)
        tempatureLabel.text = "\(String(format: "%.1f", tempature)) C"
    }
    
    func clearAllData() {
        timeLabel.text = nil
        forecastImage.image = nil
        tempatureLabel.text = nil
    }
    
}
