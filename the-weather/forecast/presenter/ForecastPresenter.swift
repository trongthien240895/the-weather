//
//  ForecastPresenter.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import Foundation
import SwiftSky

protocol ForecastPresenter: class {
    func getForecast(latitude: Double, longitude: Double)
}

class ForecastPresenterImpl: ForecastPresenter {
    
    private var view: ForecastView?
    
    init(view: ForecastView) {
        self.view = view
    }
    
    func getForecast(latitude: Double, longitude: Double) {
        SwiftSky.get([.current, .minutes, .hours, .days, .alerts],
                     at: Location(latitude: latitude, longitude: longitude)
        ) { result in
            switch result {
            case .success(let forecast):
                self.view?.onLoadSuccess(forecast)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
}
