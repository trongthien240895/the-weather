//
//  ForecastTableViewCell.swift
//  the-weather
//
//  Created by Mac Pro on 7/13/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit
import SwiftSky

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var forecastImage: UIImageView!
    @IBOutlet weak var temperatureMinLabel: UILabel!
    @IBOutlet weak var temperatureMaxLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dayLabel.text = nil
        forecastImage.image = nil
        temperatureMaxLabel.text = nil
        temperatureMinLabel.text = nil
    }
    
    func bindData(_ dataPoint: DataPoint) {
        dayLabel.text = dataPoint.time.dayOfWeek(.dayOfWeek)
        forecastImage.image = UIImage(named: "\(dataPoint.icon ?? "")")
        temperatureMinLabel.text = "\(String(format: "%.1f", dataPoint.temperature?.min?.value ?? 0))"
        temperatureMaxLabel.text = "\(String(format: "%.1f", dataPoint.temperature?.max?.value ?? 0))"
    }
    
}
