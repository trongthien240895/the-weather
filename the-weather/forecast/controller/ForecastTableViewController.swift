//
//  ForecastTableViewController.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit
import SwiftSky

class ForecastTableViewController: UITableViewController {
    
    @IBOutlet var forecastTable: UITableView!
    private var pointByHours: [DataPoint]?
    private var pointByDays: [DataPoint]?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func getHeaderTableView() -> UICollectionView {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let headerView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 100),
                                          collectionViewLayout: layout)
        headerView.isUserInteractionEnabled = true
        headerView.layer.cornerRadius = 5
        headerView.layer.masksToBounds = true
        headerView.dataSource = self
        headerView.delegate = self
        headerView.register(ForecastCollectionViewCell.self, forCellWithReuseIdentifier: "forecastCollectionCell")
        headerView.showsHorizontalScrollIndicator = false
        headerView.backgroundColor = UIColor.red
        return headerView
    }
    
    func updateForecast(_ pointByHours: [DataPoint], pointByDays: [DataPoint]) {
        self.pointByHours = Array(pointByHours.prefix(24))
        self.pointByDays = pointByDays
        forecastTable.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getHeaderTableView()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointByDays?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastTableViewCell",
                                                       for: indexPath) as? ForecastTableViewCell,
            let pointByDays = pointByDays else {
                return UITableViewCell()
            }
        cell.bindData(pointByDays[indexPath.row])
        return cell
    }

}

extension ForecastTableViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pointByHours?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "forecastCollectionCell",
                                                            for: indexPath) as? ForecastCollectionViewCell,
            let dataPoints = pointByHours else {
            return UICollectionViewCell()
        }
        cell.bindData(dataPoint: dataPoints[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 5, height: 90)
    }
    
}
