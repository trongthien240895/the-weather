//
//  ForecastCollectionViewCell.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit
import SwiftSky

class ForecastCollectionViewCell: UICollectionViewCell {
    
    var forecastTimeCell = ForecastTimeCell()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        forecastTimeCell.clearAllData()
    }
    
    func bindData(dataPoint: DataPoint) {
        contentView.addSubview(forecastTimeCell)
        forecastTimeCell.frame = contentView.frame
        guard let time = dataPoint.time.dayOfWeek(.dateType2),
            let icon = dataPoint.icon,
            let tempature = dataPoint.temperature?.current?.value else {
            return
        }
        forecastTimeCell.initCell(time, icon: icon, tempature: tempature)
    }
    
}
