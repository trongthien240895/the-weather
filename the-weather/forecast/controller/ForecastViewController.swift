//
//  ForecastViewController.swift
//  the-weather
//
//  Created by Mac Pro on 6/30/18.
//  Copyright © 2018 Mac Pro. All rights reserved.
//

import UIKit
import SwiftSky
import MapKit

protocol ForecastView {
    
    func onLoadSuccess(_ forecast: Forecast)
    func onLoadFail(_ message: String)

}

enum DayType: String {
    case day = "day"
    case night = "night"
}

class ForecastViewController: UIViewController {
    
    @IBOutlet weak var headerView: ForecastHeader!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var temperatureMaxLabel: UILabel!
    @IBOutlet weak var temperatureMinLabel: UILabel!
    
    fileprivate var locationServices: LocationServices?
    private var presenter: ForecastPresenter?
    private var forecastTable: ForecastTableViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
//        initData()
        fakeData()
    }
    
    func initView() {
        headerView.layer.cornerRadius = headerView.frame.width / 2
        headerView.layer.masksToBounds = true
        headerView.backgroundColor = .red
    }
    
    func fakeData() {
        SwiftSky.units.temperature = .celsius
        presenter = ForecastPresenterImpl(view: self)
        presenter?.getForecast(latitude: 1.1234, longitude: 1.234)
    }
    
    func initData() {
        locationServices = LocationServices.shared
        guard let coordinate = locationServices?.currentLocation?.coordinate else {
            return
        }
        presenter = ForecastPresenterImpl(view: self)
        SwiftSky.units.temperature = .celsius
        presenter?.getForecast(latitude: coordinate.latitude, longitude: coordinate.longitude)
        locationServices?.getAdress { address, error in
            if let address = address, let city = address["City"] as? String {
                self.headerView.cityLabel.text = city
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToForecastTable",
            let controller = segue.destination as? ForecastTableViewController {
            forecastTable = controller
        }
    }

}

extension ForecastViewController: ForecastView {
    
    func onLoadSuccess(_ forecast: Forecast) {
        guard let pointByHours = forecast.hours?.points,
            let pointByDay = forecast.days?.points else {
            return
        }
        bindDataHeaderView(forecast)
        forecastTable?.updateForecast(pointByHours, pointByDays: pointByDay)
    }
    
    func onLoadFail(_ message: String) {
        
    }
    
    func bindDataHeaderView(_ forecast: Forecast) {
        guard let dataPoint = forecast.current,
            let currentDayData = forecast.days?.points.first else {
            return
        }
        headerView.forecastImage.image = UIImage(named: "\(dataPoint.icon ?? "")")
        headerView.dayLabel.text = dataPoint.time.dayOfWeek(.dayOfWeek)
        headerView.dateLabel.text = dataPoint.time.dayOfWeek(.dateType1)
        headerView.temperatureLabel.text = "\(String(format: "%.1f", dataPoint.temperature?.current?.value ?? 0)) C"
        windSpeedLabel.text = "\(String(format: "%.1f", dataPoint.wind?.speed?.value ?? 0)) m/s"
        temperatureMinLabel.text = "\(String(format: "%.1f", currentDayData.temperature?.min?.value ?? 0))"
        temperatureMaxLabel.text = "\(String(format: "%.1f", currentDayData.temperature?.max?.value ?? 0))"
    }

}
